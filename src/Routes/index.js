const express = require('express')
const router = express.Router()
var mongoose = require('mongoose');
const db = mongoose.connection;

router.get('/', (request, response, next) => {
  response.render('Home.ejs')
  next();
})

// app.get('/', function(req, res){
//     res.render('home');
//   });

router.get('/registration', function (req, res, next) {
  res.render('registration.ejs');
  next();
});


router.all('/contact', function (req, res) {
  res.render('contact.ejs');
});

router.all('/conference', function (req, res, next) {
  res.render('conference.ejs');
  next();
});

router.all('/TravelAndLodging', function (req, res, next) {
  res.render('TravelAndLodging.ejs');
  next();
});

router.get('/VendorDetails', function (req, res, next) {
  res.render('VendorDetails.ejs');
  next();
});

router.all('/attendee', function (req, res, next) {
  db.collection('attendeeRegistration2').drop(function(err){
    console.log('collection dropped');  

  });
  res.render('attendee.ejs', { newCount: 0, amount: 125 });
  next();
});

router.all('/DeleteDatabase', function (req, res, next) {
  db.collection('attendeeRegistration2').drop();
  res.render('DeleteDatabase.ejs')
  next();
});

router.all('/presenter', function (req, res, next) {
  res.render('presenter.ejs');
  next();
});

router.all('/presenterInfo', function (req, res) {
  db.collection('presenterregistrations').find().toArray(function (error, result) {
    if (error) throw error;
    console.log(result);
    res.render('presenterInfo.ejs', { listOfPresenters: result });
  });
});

router.all('/proposalsInfo', function (req, res) {
  db.collection('proposals').find().toArray(function (error, result) {
    if (error) throw error;
    console.log(result);
    res.render('proposalsInfo.ejs', { listOfProposals: result });
  });
});

router.all('/student', function (req, res, next) {
  res.render('student.ejs');
  next();
});

router.get('/UpdateProgramDetails', function (req, res) {
  db.collection('breakoutsessions').find().sort({ BreakOut_Session: 1 }).toArray(function (error, result) {
    if (error) throw error;
    // console.log(result);
    res.render('UpdateProgramDetails.ejs', { breakOutSession: result });

  })
});

router.get('/RegistrationFee', function (req, res) {
  db.collection('registrationrates').find().toArray(function (error, result) {
    if (error) throw error;
    console.log(result);
    res.render('RegistrationFee.ejs', { feeDetails: result });
  });
});


router.all('/updateTable', function (req, res, next) {
  res.render('updateTable.ejs');
  next();
});
router.all('/faculty', function (req, res, next) {
  res.render('faculty.ejs');
  next();
});

router.all('/vendor', function (req, res, next) {
  res.render('vendor.ejs');
  next();
});
router.all('/presenterstatus', function (req, res, next) {
  res.render('presenterstatus.ejs');
  next();
});

router.all('/Schedule', function (req, res) {
  db.collection('breakoutsessions').find().sort({ BreakOut_Session: 1 }).toArray(function (error, result) {
    if (error) throw error;
    console.log(result)
    res.render('Schedule.ejs', { scheduleList: result });
  });
});

router.get('/payment', function (req, res, next) {
  res.render('payment.ejs', { newCount: 0 });
  next();
});

router.get('/paybycheck', function (req, res, next) {
  res.render('paybycheck.ejs');
  next();
});

router.get('/paybycard', function (req, res, next) {
  res.render('paybycard.ejs');
  next();
});

router.get('/Deadlines', function (req, res) {
  db.collection('deadlines').find().toArray(function (error, result) {
    if (error) throw error;
    console.log(result);
    res.render('Deadlines.ejs', { deadlines: result });
  });
});



router.get('/UpdateConference', function (req, res, next) {
  res.render('UpdateConference.ejs');
  next();
});

router.get('/Proposals', function (req, res, next) {
  res.render('Proposals.ejs');
  next();
});



router.get('/AttendeeInfo', function (req, res) {
  db.collection('attendeeregistrations').find().toArray(function (error, result) {
    if (error) throw error;
    console.log(result);
    res.render('AttendeeInfo.ejs', { listOfAttendees: result });
  });
});

//Student Information
router.get('/studentInfo', function (req, res) {
  db.collection('studentregistrations').find().toArray(function (error, result) {
    if (error) throw error;
    console.log(result);
    res.render('studentInfo.ejs', { listOfStudents: result });
  });
});

router.all('/studentPayByCheck', function (req, res) {
  db.collection('registrationrates').findOne({registrationRate : 'Student'}, function(err, regFee){
    res.render('studentPayByCheck.ejs', {name :req.body.name, amount:regFee.registrationFee });
  })
});

router.all('/presenterPayByCheck', function (req, res) {
  db.collection('registrationrates').findOne({registrationRate : 'Presenter'}, function(err, regFee){
    res.render('studentPayByCheck.ejs', {name :req.body.name, amount:regFee.registrationFee });
  })
});

router.all('/vendorPayByCheck', function (req, res) {
  db.collection('registrationrates').findOne({registrationRate : req.body.type}, function(err, regFee){
    res.render('studentPayByCheck.ejs', {name :req.body.name, amount:regFee.registrationFee });
  })
});




router.get('/vendorinfo', function (req, res) {
  db.collection('vendorregistrations').find().toArray(function (error, result) {
    if (error) throw error;
    console.log(result);
    res.render('vendorinfo.ejs', { listOfVendors: result });
  });
});

router.all('/PaymentDetails', function (req, res) {
  db.collection('paymentStatus').find().toArray(function (error, result) {
    if (error) throw error;
    console.log(result);
    res.render('paymentStatusInfo.ejs', { listOfPaymentStatus: result });
  });
});

router.get('/participants', function (req, res) {
  db.collection('attendeeregistrations').find().toArray(function (error, attendees) {
    if (error) throw error;
    // res.render('participants.ejs', { listOfParticipants: result});
    db.collection('vendorregistrations').find().toArray(function (error, vendors) {
      if (error) throw error;

    db.collection('studentregistrations').find().toArray(function (error, students) {
        if (error) throw error;

    db.collection('presenterregistrations').find().toArray(function (error, presenters) {
          if (error) throw error;

    db.collection('facultyregistrations').find().toArray(function (error, faculty) {
            if (error) throw error;

            res.render('participants.ejs', { attendeeList: attendees, vendorList: vendors, studentList:students,
            presenterList:presenters, facultyList:faculty });
          });
        });
      });
    });
  });
        // res.render('participants.ejs');

      });

      router.get('/UpdateDeadlines', function (req, res) {
        db.collection('deadlines').find().toArray(function (error, result) {
          if (error) throw error;
          // console.log(result);
          res.render('UpdateDeadlines.ejs', { deadlines: result });
        });
      });

      router.get('/UpdateFeeDetails', function (req, res) {
        db.collection('registrationrates').find().toArray(function (error, result) {
          if (error) throw error;
          console.log(result);
          res.render('UpdateFeeDetails.ejs', { feeDetails: result });
        });
      });

      router.get('/admin', ensureAuthenticated, function (req, res) {
        res.render('login');
      });

      router.get('/adminHome', function (req, res) {
        res.render('adminHome.ejs');
      });

      router.get('/forgotemail', function(req,res){
        res.render('forgotemail');

      });

      

      function ensureAuthenticated(req, res, next) {
        if (req.isAuthenticated()) {
          return next();
        } else {
          //req.flash('error_msg','You are not logged in');
          res.redirect('/users/login');
        }
      }



      module.exports = router
