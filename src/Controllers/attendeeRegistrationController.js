var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var express = require("express");
var router = express.Router();

var attendeeRegistration = require('../Schemas/attendeeRegistration');
var breakOutSession = require('../Schemas/breakOutSession');
var registrationRates = require('../Schemas/RegistrationRates');
var db = mongoose.connection;

var ObjectId = require('mongodb').ObjectID;


router.all('/payment', function (req, res) {
  // console.log('entered into post');

  var count = req.body.Count;
  // console.log(count)
  var newAttendeeRegistration = new attendeeRegistration();
  var newAttendeeRegistration2 = new attendeeRegistration();

  newAttendeeRegistration.firstName = req.body.firstName;
  newAttendeeRegistration.lastName = req.body.lastName;
  newAttendeeRegistration.email = req.body.email;
  newAttendeeRegistration.contactNumber = req.body.contactNumber;
  newAttendeeRegistration.address = req.body.address;
  newAttendeeRegistration.institutionName = req.body.institutionName;
  newAttendeeRegistration.institutionCity = req.body.institutionCity;
  newAttendeeRegistration.institutionState = req.body.institutionState;
  newAttendeeRegistration.zipcode = req.body.zipcode;
  newAttendeeRegistration.country = req.body.country;
  newAttendeeRegistration.positionTitle = req.body.positionTitle;
  newAttendeeRegistration.dietaryRestrictions = req.body.dietaryRestrictions;
  newAttendeeRegistration.programs = req.body.programs;
  newAttendeeRegistration.payment = req.body.payment;
   
  var qty = req.body.paperProceedings

  var isExists = db.collection('attendeeregistrations').find({ 'email': req.body.email }).count();

  isExists.then(function (result) {
    if (result == 0) {
      newAttendeeRegistration.save().then(item => {
        let amount = db.collection('registrationrates').find({'registrationRate':"Regular Rate"});
        // console.log(amount.registrationFee)
        db.collection('attendeeregistrations').find().toArray(function (err, result) {
          if (err) throw err;
          res.render('payment.ejs', { newCount: parseInt(count), amount: 125, qty: qty, amnt:20 });
        })
      })
        .catch(err => {
          res.status(400).send("unable to save to database");
        });
    } else
      res.send('Email is already registered')
  })


  newAttendeeRegistration2.firstName = req.body.firstName;
  newAttendeeRegistration2.lastName = req.body.lastName;
  newAttendeeRegistration2.email = req.body.email;
  newAttendeeRegistration2.contactNumber = req.body.contactNumber;
  newAttendeeRegistration2.address = req.body.address;
  newAttendeeRegistration2.institutionName = req.body.institutionName;
  newAttendeeRegistration2.institutionCity = req.body.institutionCity;
  newAttendeeRegistration2.institutionState = req.body.institutionState;
  newAttendeeRegistration2.zipcode = req.body.zipcode;
  newAttendeeRegistration2.country = req.body.country;
  newAttendeeRegistration2.positionTitle = req.body.positionTitle;
  newAttendeeRegistration2.dietaryRestrictions = req.body.dietaryRestrictions;
  newAttendeeRegistration2.programs = req.body.programs;
  newAttendeeRegistration2.paperProceedings = req.body.paperProceedings;

  // console.log(req.body.firstName)
  // console.log(newAttendeeRegistration2);
  db.collection('attendeeRegistration2').insertOne({
    Attendee_FirstName: newAttendeeRegistration2.firstName,
    Attendee_LastName: newAttendeeRegistration2.lastName,
    Email_Address: newAttendeeRegistration2.email,
    Contact_Number: newAttendeeRegistration2.contactNumber,
    Address: newAttendeeRegistration2.address,
    Institution_Name: newAttendeeRegistration2.institutionName,
    Institution_City: newAttendeeRegistration2.institutionCity,
    Institution_State: newAttendeeRegistration2.institutionState,
    Zipcode: newAttendeeRegistration2.zipcode,
    Country: newAttendeeRegistration2.country,
    Position_Title: newAttendeeRegistration2.positionTitle,
    Job_Title: newAttendeeRegistration2.Job_Title,
    Dietary_Restrictions: newAttendeeRegistration2.Dietary_Restrictions,
    programs: newAttendeeRegistration2.Choose_Program,
    Paper_Proceedings: newAttendeeRegistration2.paperProceedings
  }, function (err, result) {
    if (err) {
      res.send('Problem in updating');
    } else {

    }
  });



});

router.all('/attendee', function (req, res) {
  var newAttendeeRegistration = new attendeeRegistration();
  var newAttendeeRegistration2 = new attendeeRegistration();
  var count = req.body.Count;
  // console.log(count)

  newAttendeeRegistration.firstName = req.body.firstName;
  newAttendeeRegistration.lastName = req.body.lastName;
  newAttendeeRegistration.email = req.body.email;
  newAttendeeRegistration.contactNumber = req.body.contactNumber;
  newAttendeeRegistration.address = req.body.address;
  newAttendeeRegistration.institutionName = req.body.institutionName;
  newAttendeeRegistration.institutionCity = req.body.institutionCity;
  newAttendeeRegistration.institutionState = req.body.institutionState;
  newAttendeeRegistration.zipcode = req.body.zipcode;
  newAttendeeRegistration.country = req.body.country;
  newAttendeeRegistration.positionTitle = req.body.positionTitle;
  newAttendeeRegistration.dietaryRestrictions = req.body.dietaryRestrictions;
  newAttendeeRegistration.programs = req.body.programs;
  newAttendeeRegistration.payment = req.body.payment;

  newAttendeeRegistration.save(function (err, registration) {
    if (err) {
      res.send('Error in saving');
    } else {
      // console.log(registration);
      // res.redirect('/payment')
      if (count == 5) {
        res.render('payment.ejs', { newCount: count });
      }
      else {
        res.render('attendee.ejs', { newCount: count });
      }

    }

    var isExists = db.collection('attendeeregistrations').find({ 'email': req.body.email }).count();

    isExists.then(function (result) {
      if (result == 0) {
        newAttendeeRegistration.save().then(item => {
          let amount = db.collection('registrationrates').findOne({'registrationRate':"Regular Rate"});
          db.collection('attendeeregistrations').find().toArray(function (err, result) {
            if (err) throw err;
            if(count==5){
            res.render('payment.ejs', { newCount: count, amount:parseInt(amount.registrationFee) });
            }
            else{
              res.render('attendee.ejs', { newCount: count, amount:amount });
            }
          })
        })
          .catch(err => {
            res.status(400).send("unable to save to database");
          });
      } else
        res.send('Email is already registered')
    })
  });


  newAttendeeRegistration2.firstName = req.body.firstName;
  newAttendeeRegistration2.lastName = req.body.lastName;
  newAttendeeRegistration2.email = req.body.email;
  newAttendeeRegistration2.contactNumber = req.body.contactNumber;
  newAttendeeRegistration2.address = req.body.address;
  newAttendeeRegistration2.institutionName = req.body.institutionName;
  newAttendeeRegistration2.institutionCity = req.body.institutionCity;
  newAttendeeRegistration2.institutionState = req.body.institutionState;
  newAttendeeRegistration2.zipcode = req.body.zipcode;
  newAttendeeRegistration2.country = req.body.country;
  newAttendeeRegistration2.positionTitle = req.body.positionTitle;
  newAttendeeRegistration2.dietaryRestrictions = req.body.dietaryRestrictions;
  newAttendeeRegistration2.programs = req.body.programs;
  newAttendeeRegistration2.paperProceedings = req.body.paperProceedings;

  // console.log(newAttendeeRegistration2);
  db.collection('attendeeRegistration2').insertOne({
    Attendee_FirstName: newAttendeeRegistration2.firstName,
    Attendee_LastName: newAttendeeRegistration2.lastName,
    Email_Address: newAttendeeRegistration2.email,
    Contact_Number: newAttendeeRegistration2.contactNumber,
    Address: newAttendeeRegistration2.address,
    Institution_Name: newAttendeeRegistration2.institutionName,
    Institution_City: newAttendeeRegistration2.institutionCity,
    Institution_State: newAttendeeRegistration2.institutionState,
    Zipcode: newAttendeeRegistration2.zipcode,
    Country: newAttendeeRegistration2.country,
    Position_Title: newAttendeeRegistration2.positionTitle,
    Job_Title: newAttendeeRegistration2.Job_Title,
    Dietary_Restrictions: newAttendeeRegistration2.Dietary_Restrictions,
    programs: newAttendeeRegistration2.Choose_Program,
    Paper_Proceedings: newAttendeeRegistration2.paperProceedings
  }, function (err, result) {
    if (err) {
      res.send('Problem in updating');
    } else {

    }
  });

});



router.all('/UpdateProgramDetails', function (req, res) {
  // console.log('entered into post');
  // console.log(req.body);

  var newBreakOutSession = new breakOutSession();

  newBreakOutSession.BreakOut_Session = req.body.breakOut;
  newBreakOutSession.Time = req.body.time;
  newBreakOutSession.Activity = req.body.activity;
  newBreakOutSession.Description = req.body.description;
  newBreakOutSession.Location = req.body.location;

  // console.log(newBreakOutSession.BreakOut_Session);

  // db.collection('breakoutsessions').save(newBreakOutSession, (err, result) =>{
  //   if(err) return console.log(err)
  //   return res.redirect('/UpdateProgramDetails');
  // })


  newBreakOutSession.save(function (err, result) {
    if (err) {
      res.send('Error in saving');
    } else {
      // console.log(newBreakOutSession);
      // res.redirect('/payment')
      // res.render('UpdateProgramDetails.ejs');
      res.redirect('/UpdateProgramDetails');

    }
  });

});

router.all('/deleteProgram', function (req, res) {
  var query = { "_id": ObjectId(req.body.perfId2) };
  db.collection('breakoutsessions').deleteOne(query, function (err, result) {
    if (err) throw err;
    return res.redirect('/UpdateProgramDetails')
    // res.render('form.ejs', { perf : result});
    // res.redirect('Homepage.ejs',{listOfPerformers : result});
  });
});


var ObjectID = require('mongodb').ObjectID;

router.all('/editProgram', function (req, res) {

  var newBreakOutSession = new breakOutSession();

  newBreakOutSession.BreakOut_Session = req.body.breakOut;
  newBreakOutSession.Time = req.body.Time;
  newBreakOutSession.Activity = req.body.Activity;
  newBreakOutSession.Description = req.body.Description;
  newBreakOutSession.Location = req.body.Location;

  db.collection('breakoutsessions').updateOne(
    { "_id": ObjectId(req.body.editId) },
    {
      $set: {
        "BreakOut_Session": newBreakOutSession.BreakOut_Session, "Time": newBreakOutSession.Time, "Activity": newBreakOutSession.Activity, "Description": newBreakOutSession.Description,
        "Location": newBreakOutSession.Location
      }
    }, function (err, result) {
      if (err) { throw err; }
      // console.log(result);
      res.redirect('/UpdateProgramDetails');
    });

});






module.exports = router;
