var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var express = require("express");
var router = express.Router();
var db = mongoose.connection;

var presenterRegistration = require('../Schemas/presenterRegistration');

router.all('/payment', function (req, res) {
    console.log('entered into post');
    var newPresenterRegistration = new presenterRegistration();
  
    newPresenterRegistration.Presenter_FirstName = req.body.Presenter_FirstName;
    newPresenterRegistration.Presenter_LastName = req.body.Presenter_LastName;
    newPresenterRegistration.Presenter_Email_Address = req.body.Presenter_Email_Address;
    newPresenterRegistration.Presenter_Contact_Number = req.body.Presenter_Contact_Number;
    newPresenterRegistration.Presenter_Address = req.body.Presenter_Address;
    newPresenterRegistration.Presenter_Position_Title = req.body.Presenter_Position_Title;
    newPresenterRegistration.Presenter_Dietary_Restrictions = req.body.Presenter_Dietary_Restrictions;
    newPresenterRegistration.programs = req.body.programs;
    newPresenterRegistration.payment = req.body.payment;
  
    newPresenterRegistration.save(function (err, registration) {
  
      if (err) {
        res.send('Error in saving');
      } else {
        console.log(registration);
        // res.send(registration);
        res.render('presenterPayment.ejs', {name:req.body.Presenter_FirstName + " "+req.body.Presenter_LastName});
      }
    });
  });


  router.all('/paybycheck', function (req, res) {

    var arra = 0;
    db.collection('attendeeRegistration2').find().toArray(function(err,result){
      if(err) throw err;
      console.log(result.length);
      arra = result.length;
      console.log(arra);
      
      var name = [];
      var paperProceedings = [];
      for(var j = 0; j < result.length ; j++){
         name.push(result[j].Attendee_FirstName);
        paperProceedings.push(result[j].Paper_Proceedings);
      }
      var paperProceedingCount = 0;
      for (var i = 0; i < paperProceedings.length; i++) {
        paperProceedingCount += parseInt(paperProceedings[i])
      }
      console.log(paperProceedingCount)
      console.log(name);
      db.collection('registrationrates').findOne({registrationRate : 'Attendee'}, function(err, attendeeRegistration){
      attendeeRegistrationCost = attendeeRegistration.registrationFee

      db.collection('registrationrates').findOne({registrationRate : 'Paper Proceedings'}, function(err, paperProceedings){
      paperProceedingsCost = paperProceedings.registrationFee

      var totalAmount = (attendeeRegistrationCost * arra) + (paperProceedingCount * paperProceedingsCost)
      
      res.render('paybycheck.ejs', {count: arra, nameArray: name, proceedings: paperProceedingCount, 
        attendeeRegistrationCost: attendeeRegistrationCost, paperProceedingsCost: paperProceedingsCost, totalAmount: totalAmount});
    })
  })
})
  
  });

  router.all('/studentPayByCheck', function (req, res) {

    
     
  
  });

  module.exports = router;