var path = require("path");
var express = require("express");
var logger = require("morgan");
var mongo = require('mongodb');
const favicon = require('serve-favicon');
var bodyParser = require("body-parser"); // simplifies access to request body
var app = express();  // make express app
var http = require('http').Server(app);  // inject app into the server
var router = express.Router();
var nodemailer = require('nodemailer');
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: false }));
var async = require("async");
var crypto = require("crypto");
var bcrypt = require("bcryptjs");

var cookieParser = require('cookie-parser');
var exphbs = require('express-handlebars');
var expressValidator = require('express-validator');
var flash = require('connect-flash');
var session = require('express-session');
var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;

var attendeeRegistrations = require('./Controllers/attendeeRegistrationController');
var presenterRegistrations = require('./Controllers/presenterRegistrationController');
var ObjectId = require('mongodb').ObjectID;


//Save deadlines in database
var deadlines = require('./Schemas/deadlines');
var User = require('./Schemas/user.js')

//Save Registration rates in database
var registrationRates = require('./Schemas/RegistrationRates.js');
// Express Session
app.use(session({
  secret: 'secret',
  saveUninitialized: true,
  resave: true
}));

// Passport init
app.use(passport.initialize());
app.use(passport.session());

// Express Validator
app.use(expressValidator({
  errorFormatter: function (param, msg, value) {
    var namespace = param.split('.')
      , root = namespace.shift()
      , formParam = root;

    while (namespace.length) {
      formParam += '[' + namespace.shift() + ']';
    }
    return {
      param: formParam,
      msg: msg,
      value: value
    };
  }
}));

// Connect Flash
app.use(flash());

// Global Vars
app.use(function (req, res, next) {
  res.locals.success_msg = req.flash('success_msg');
  res.locals.error_msg = req.flash('error_msg');
  res.locals.error = req.flash('error');
  res.locals.user = req.user || null;
  next();
});






//Import the mongoose module
var mongoose = require('mongoose');

var studentRegistration = require('./Schemas/studentRegistration');
var facultyRegistration = require('./Schemas/facultyRegistration');
var vendorRegistration = require('./Schemas/vendorRegistration');
var proposalSubmissions = require('./Schemas/proposalSchema');
//Set up default mongoose connection
var db = 'mongodb://localhost:27017/test';
mongoose.connect(db, { useNewUrlParser: true });
const dbo = mongoose.connection;
// Get Mongoose to use the global promise library
mongoose.Promise = global.Promise;


router.all('/studentRegistration', function (req, res) {
  console.log('entered into post');
  var newStudentRegistration = new studentRegistration();

  newStudentRegistration.firstName = req.body.firstName;
  newStudentRegistration.lastName = req.body.lastName;
  newStudentRegistration.email = req.body.email;
  newStudentRegistration.contactNumber = req.body.contactNumber;
  newStudentRegistration.address = req.body.address;
  newStudentRegistration.institutionName = req.body.institutionName;
  newStudentRegistration.institutionCity = req.body.institutionCity;
  newStudentRegistration.institutionState = req.body.institutionState;
  newStudentRegistration.zipcode = req.body.zipcode;
  newStudentRegistration.country = req.body.country;
  newStudentRegistration.dietaryRestrictions = req.body.dietaryRestrictions;
  newStudentRegistration.programs = req.body.programs;
  newStudentRegistration.payment = req.body.payment;

  var isExists = dbo.collection('studentregistrations').find({ 'email': req.body.email }).count();

  isExists.then(function (result) {
    if (result == 0) {
      var tempObjCollection = dbo.collection('registrationrates').find({});
      tempObjCollection.forEach(obj => {
        var temp = obj.registrationRate
        if (temp === "Student") {

          console.log(obj.registrationFee)
          newStudentRegistration.save().then(item => {
            dbo.collection('studentregistrations').find().toArray(function (err, result) {
              if (err) throw err;
              res.render('paymentOption.ejs', { name: req.body.firstName+" "+req.body.lastName});
            })
          })
            .catch(err => {
              res.status(400).send("unable to save to database");
            });
        }
      });

    } else
      res.send('Email is already registered')
  })
});


router.all('/facultyRegistration', function (req, res) {
  console.log('entered into post');
  var newFacultyRegistration = new facultyRegistration();

  newFacultyRegistration.validate_emailId = text;
  newFacultyRegistration.firstName = req.body.firstName;
  newFacultyRegistration.lastName = req.body.lastName;
  newFacultyRegistration.email = req.body.email;
  newFacultyRegistration.contactNumber = req.body.contactNumber;
  newFacultyRegistration.address = req.body.address;
  newFacultyRegistration.institutionName = req.body.institutionName;
  newFacultyRegistration.institutionCity = req.body.institutionCity;
  newFacultyRegistration.institutionState = req.body.institutionState;
  newFacultyRegistration.zipcode = req.body.zipcode;
  newFacultyRegistration.country = req.body.country;
  newFacultyRegistration.positionTitle = req.body.positionTitle;
  newFacultyRegistration.dietaryRestrictions = req.body.dietaryRestrictions;
  newFacultyRegistration.programs = req.body.programs;

  var isExists = dbo.collection('facultyregistrations').find({ 'email': req.body.email }).count();

  isExists.then(function (result) {
    if (result == 0) {
      newFacultyRegistration.save().then(item => {
        res.render('facultystatus.ejs');
      })
        .catch(err => {
          res.status(400).send("unable to save to database");
        });
    } else
      res.send('Email is already registered')
  })
});

router.all('/vendorRegistration', function (req, res) {
  console.log('entered into post');
  var newVendorRegistration = new vendorRegistration();
  var vendor1 = req.body.VendorType;
  newVendorRegistration.VendorType = req.body.VendorType;
  newVendorRegistration.companyName = req.body.companyName;
  newVendorRegistration.city = req.body.city;
  newVendorRegistration.state = req.body.state;
  newVendorRegistration.zipcode = req.body.zipcode;
  newVendorRegistration.websiteURL = req.body.websiteURL;
  newVendorRegistration.contactName = req.body.contactName;
  newVendorRegistration.contactTitle = req.body.contactTitle;
  newVendorRegistration.contactPhone = req.body.contactPhone;
  newVendorRegistration.contactEmail = req.body.contactEmail;
  newVendorRegistration.confirmEmail = req.body.confirmEmail;
  newVendorRegistration.dietaryRestrictions = req.body.dietaryRestrictions;
  newVendorRegistration.programs = req.body.programs;
  newVendorRegistration.payment = req.body.payment;

  var isExists = dbo.collection('vendorregistrations').find({ 'contactEmail': req.body.contactEmail }).count();

  isExists.then(function (result) {
    console.log(result);
    console.log(req.body.vendorType);
    if (result == 0) {
      newVendorRegistration.save().then(item => {

        if (vendor1 == "Gray") {
          res.render('vendorPayment.ejs', { name: req.body.contactName, type:"Vendor(Gray)"});
        }
        else if (vendor1 == "White") {
          res.render('vendorPayment.ejs', { name: req.body.contactName, type:"Vendor(White)"});
        }
        else {
          res.render('vendorPayment.ejs', { name: req.body.contactName, type:"Vendor(Green)"});
        }
      })
        .catch(err => {
          res.status(400).send("unable to save to database");
        });
    } else
      res.send('Email is already registered')
  })
});


app.post('/uploadFile', function (req, res) {

  var form = new formidable.IncomingForm();
  form.parse(req, function (err, fields, files) {
    var oldPath = files.filetoupload.path;
    var newPath = 'files/' + files.filetoupload.name;
    fs.rename(oldPath, newPath, function (err) {
      if (err) throw err;
      console.log('Sapving');
    })
  })
});

router.all('/proposalsubmission', function (req, res) {
  console.log('entered into post');
  var newProposal = new proposalSubmissions();

  newProposal.firstName = req.body.firstName;
  newProposal.lastName = req.body.lastName;
  newProposal.positionTitle = req.body.positionTitle;
  newProposal.institution = req.body.institution;
  newProposal.city = req.body.city;
  newProposal.state = req.body.state;
  newProposal.country = req.body.country;
  newProposal.zipcode = req.body.zipcode;
  newProposal.emailAddress = req.body.emailAddress;
  newProposal.contactNumber = req.body.contactNumber;
  newProposal.equimentNeeds = req.body.equimentNeeds;
  newProposal.softwareNeeds = req.body.softwareNeeds;
  newProposal.paper = req.body.paper;
  newProposal.sessionTitle = req.body.sessionTitle;
  newProposal.description = req.body.description;
  newProposal.dietaryRestrictions = req.body.dietaryRestrictions;
  newProposal.track = req.body.track;
  newProposal.accept = req.body.accept;
  newProposal.reject = req.body.reject;
  newProposal.filepath = 'files/RE_ GDP Project - Team Tesla.pdf';
  console.log(req.body.emailAddress);
  var isExists = dbo.collection('proposals').find({ 'emailAddress': req.body.emailAddress }).count();

  isExists.then(function (result) {
    console.log(result);
    if (result == 0) {

      newProposal.save().then(item => {
        dbo.collection('proposals').find().toArray(function (err, result) {
          if (err) throw err;

          res.send('Your proposal is Successfully submitted. Please wait for the confrmation');
        })
      })
        .catch(err => {
          res.status(400).send("unable to save to database");
        });
    } else
      res.send('Email is already registered')
  })
});


var text = "";
var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

for (var i = 0; i < 6; i++) {
  text += possible.charAt(Math.floor(Math.random() * possible.length));
}
app.post('/send', (req, res) => {
  console.log(req.body.emailId);


  var transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
      user: 'teamtesla02@gmail.com',
      pass: 'gdp02gdp02'
    }
  });

  var mailOptions = {
    from: 'teamtesla02',
    to: req.body.emailId,
    subject: 'Coupon code for .Edu Conference Registration',
    html: 'Congratulations!!! You are verified. <br> Here is the coupon code for you: ' + text

  };

  transporter.sendMail(mailOptions, function (error, info) {
    if (error) {
      console.log(error);
    } else {
      console.log('Email sent: ' + info.response);
      // res.send("Your verification code is on your way check your email");
      res.render('faculty.ejs', { code: text });
    }
  });
});

app.get('/getData', function (req, res) {
  Registration.find({})
    .exec(function (error, registrations) {
      if (error) {
        res.send("Error has occured");
      } else {
        console.log(registrations);
        res.json(registrations);
        // res.send(registrations);

      }
    });

});




//Save new added program and its deadline to the database

app.post("/newDeadline", (req, res) => {

  var myData = new deadlines();
  var dateObject = new Date(req.body.date);
  myData.programName = req.body.programName;
  // console.log(req.body.date);
  // myData.date = ""+req.body.date;
  myData.date = dateObject.toLocaleDateString('en-US') + " " + dateObject.toLocaleTimeString('en-US');
  // console.log(myData.date);
  myData.save()
    .then(item => {
      // console.log(req.body)
      //  res.send("Hello")
      res.redirect('/UpdateDeadlines');
    })
    .catch(err => {
      res.status(400).send("unable to save to database");
    });

});

// Delete record from Database
app.post("/deletedeadline", function (request, response) {
  var query = { "_id": ObjectId(request.body.selectedId) };
  dbo.collection('deadlines').deleteOne(query, function (err, result) {
    response.redirect('/UpdateDeadlines')
  });
});

app.post("/modifydeadline", function (req, res) {
  var dateObject = new Date(req.body.date);
  console.log(req.body.programName);
  var query = { "programName": req.body.programName }
  var newValues = { $set: { date: (dateObject.toLocaleDateString('en-US') + " " + dateObject.toLocaleTimeString('en-US')) } };
  console.log(req.body.date);
  console.log(query);
  console.log(newValues);
  dbo.collection('deadlines').updateOne(query, newValues, function (err, res) {
    if (err) throw err;
    console.log("1 document updated");
  });
  res.redirect('/UpdateDeadlines');
});


app.post("/modifyFeeDetails", function (req, res) {
  var query = { "registrationRate": req.body.registrationRate }
  var newValues = { $set: { registrationFee: req.body.registrationFee, whatIncluded: req.body.whatIncluded } };
  dbo.collection('registrationrates').updateOne(query, newValues, function (err, res) {
    if (err) throw err;
    console.log("1 document updated");
  });
  res.redirect('/UpdateFeeDetails');
});

// Delete record from Database
app.post("/deleteFeeDetails", function (request, response) {
  var query = { "_id": ObjectId(request.body.selectedId) };
  dbo.collection('registrationrates').deleteOne(query, function (err, result) {
    response.redirect('/UpdateFeeDetails')
  });
});


//Send mail to presenter if the proposal is confirmed
app.post("/accept", function (request, response) {
  dbo.collection('proposals').updateOne({ 'emailAddress': request.body.email1 }, { $set: { 'accept': "Accepted" } });
  var transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
      user: 'teamtesla02@gmail.com',
      pass: 'gdp02gdp02'
    }
  });

  var mailOptions = {
    from: 'teamtesla02@gmail.com',
    to: request.body.email1,
    subject: 'Acceptance from .EDU Conference.',
    html: '<p>Hello,</p><p>Your application as a presenter to .EDU Conference is successfully accepted.</p><p><a href="http://127.0.0.1:8082/presenter">Click here</a> to register to the conference.</p><p>Thanks&Regards</p><p>.edu team</p> ',
  };
  console.log(request.body.email1);
  transporter.sendMail(mailOptions, function (error, info) {
    if (error) {
      console.log(error);
    } else {
      console.log('Email sent: ' + info.response);
      dbo.collection('proposals').find().toArray(function (err, result) {
        if (err) throw err;
        response.render('proposalsInfo.ejs', { listOfProposals: result });
      });
    }
  });
});


// Reject Proposal

app.post("/reject", function (request, response) {
  dbo.collection('proposals').updateOne({ 'emailAddress': request.body.email1 }, { $set: { 'reject': "Rejected" } });
  var transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
      user: 'teamtesla02@gmail.com',
      pass: 'gdp02gdp02'
    }
  });

  var mailOptions = {
    from: 'teamtesla02@gmail.com',
    to: request.body.email1,
    subject: 'Reject from .EDU Conference.',
    html: '<p>Hello,</p><p>We are sorry to inform you that, your application as a presenter to .EDU Conference is rejected.</p><p>Thanks&Regards</p><p>.edu team</p> ',
  };
  transporter.sendMail(mailOptions, function (error, info) {
    if (error) {
      console.log(error);
    } else {
      console.log('Email sent: ' + info.response);
      dbo.collection('proposals').find().toArray(function (err, result) {
        if (err) throw err;
        response.render('proposalsInfo.ejs', { listOfProposals: result });

      });
    }
  });
});



// Reject Proposal

app.post("/paid", function (request, response) {
  dbo.collection('attendeeregistrations').updateOne({ 'email': request.body.email1 }, { $set: { 'payment': "Paid" } });
  dbo.collection('attendeeregistrations').find().toArray(function (err, result) {
    if (err) throw err;
    response.render('attendeeInfo.ejs', { listOfAttendees: result });

  });
});


app.post("/studentpayment", function (request, response) {
  dbo.collection('studentregistrations').updateOne({ 'email': request.body.email1 }, { $set: { 'payment': "Paid" } });
  dbo.collection('studentregistrations').find().toArray(function (err, result) {
    if (err) throw err;
    response.render('studentInfo.ejs', { listOfStudents: result });

  });
});

app.post("/presenterpayment", function (request, response) {
  dbo.collection('presenterregistrations').updateOne({ 'Presenter_Email_Address': request.body.email1 }, { $set: { 'payment': "Paid" } });
  dbo.collection('presenterregistrations').find().toArray(function (err, result) {
    if (err) throw err;
    response.render('presenterInfo.ejs', { listOfPresenters: result });
  });
});

app.post("/vendorpayment", function (request, response) {
  dbo.collection('vendorregistrations').updateOne({ 'contactEmail': request.body.email1 }, { $set: { 'payment': "Paid" } });
  dbo.collection('vendorregistrations').find().toArray(function (err, result) {
    if (err) throw err;
    response.render('vendorinfo.ejs', { listOfVendors: result });
  });
});


//Save new registration rate and fee to the database

app.post("/newRate", (req, res) => {

  var myData = new registrationRates(req.body);
  myData.save()
    .then(item => {
      console.log(req.body)
      //  res.send("Hello")
      res.redirect('/UpdateFeeDetails');
    })
    .catch(err => {
      res.status(400).send("unable to save to database");
    });

});


app.post("/send1", function (request, response) {
  db.collection('presenterregistration').update({ 'email1': request.body.email1 }, { $set: { 'accept': "Accepted" } });
  res.send("h")
});

// set up the view engine
// app.set('view engine', 'ejs'); // specify view engine
app.set("views", path.join(__dirname, "Views")); // path to views

app.engine('handlebars', exphbs({ defaultLayout: 'layout' }));
app.set('view engine', 'handlebars', 'ejs');


// set static path
app.use(router);
app.use(express.static(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'assets')));

var users = require('./routes/users');
app.use('/users', users);
const routes = require('./Routes/index.js')
app.use('/', routes)
app.use('/attendeeRegistration', attendeeRegistrations)
app.use('/presenterRegistration', presenterRegistrations)


// set up an http request logger to log every request automagically
app.use(logger("dev"));     // app.use() establishes middleware functions


//----Admin----

// Express Session
app.use(session({
  secret: 'secret',
  saveUninitialized: true,
  resave: true
}));

//forgot pass

app.post('/forgot', function (req, res, next) {
  async.waterfall([
    function (done) {
      crypto.randomBytes(20, function (err, buf) {
        var token = buf.toString('hex');
        done(err, token);
      });
    },
    function (token, done) {
      User.findOne({ email: req.body.email }, function (err, user) {
        if (!user) {
          req.flash('error_msg', 'No account with that email address exists.');
          return res.redirect('back');
        }

        user.resetPasswordToken = token;
        user.resetPasswordExpires = Date.now() + 3600000; // 1 hour

        user.save(function (err) {
          done(err, token, user);
        });
      });
    },
    function (token, user, done) {
      var smtpTransport = nodemailer.createTransport({
        service: 'gmail',
        auth: {
          user: 'teamtesla02@gmail.com',
          pass: 'gdp02gdp02'
        }
      });
      var mailOptions = {
        to: req.body.email,
        from: 'passwordreset@demo.com',
        subject: 'Node.js Password Reset',
        text: 'You are receiving this because you (or someone else) have requested the reset of the password for your account.\n\n' +
          'Please click on the following link, or paste this into your browser to complete the process:\n\n' +
          'http://' + req.headers.host + '/reset/' + token + '\n\n' +
          'If you did not request this, please ignore this email and your password will remain unchanged.\n'
      };
      smtpTransport.sendMail(mailOptions, function (err) {
        req.flash('success_msg', 'An e-mail has been sent to ' + req.body.email + ' with further instructions.');
        done(err, 'done');
      });
    }
  ], function (err) {
    if (err) return next(err);
    res.redirect('/forgotemail');
  });
});

//Reset Get
app.get('/reset/:token', function (req, res) {
  User.findOne({ resetPasswordToken: req.params.token, resetPasswordExpires: { $gt: Date.now() } }, function (err, user) {
    if (!user) {
      req.flash('error', 'Password reset token is invalid or has expired.');
      return res.redirect('/forgotemail');
    }
    res.render('forgotpassword.ejs', {
      token: req.params.token
    });
  });
});

//Reset
app.post('/reset/:token', function (req, res) {
  async.waterfall([
    function (done) {
      User.findOne({ resetPasswordToken: req.body.token, resetPasswordExpires: { $gt: Date.now() } }, function (err, user) {
        if (!user) {

          req.flash('error', 'Password reset token is invalid or has expired.');
          return res.redirect('/users/login');
        }
        //console.log(user);

        bcrypt.genSalt(10, function (err, salt) {
          bcrypt.hash(req.body.password, salt, function (err, hash) {
            user.password = hash;
            user.resetPasswordToken = undefined;
            user.resetPasswordExpires = undefined;
            user.save(function (err) {
              req.logIn(user, function (err) {
                done(err, user);
              });
            });
          });
        });




      });
    },
    function (user, done) {
      var smtpTransport = nodemailer.createTransport({
        service: 'SendGrid',
        auth: {
          user: 'gdp2.fastrack@gmail.com',
          pass: 'gdp21234'
        }
      });
      var mailOptions = {
        to: req.user.email,
        from: 'passwordreset@demo.com',
        subject: 'Your password has been changed',
        text: 'Hello,\n\n' +
          'This is a confirmation that the password for your account ' + req.user.email + ' has just been changed.\n'
      };
      smtpTransport.sendMail(mailOptions, function (err) {
        req.flash('success', 'Success! Your password has been changed.');
        done(err);
      });
    }
  ], function (err) {
    res.redirect('/users/login');
  });
});

// Passport init
app.use(passport.initialize());
app.use(passport.session());

http.listen(8082, function () {
  console.log('Listening on http://127.0.0.1:8082/');
});

