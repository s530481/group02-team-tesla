<?php
//--------------------------Set these paramaters--------------------------

// Subject of email sent to you.
$subject = 'Website Enquiry';

// Your email address. This is where the form information will be sent.
$emailadd = 'myemail@myemail.co.uk';

// Where to redirect after form is processed.
$url = 'thanks.php';

// Makes all fields required. If set to '1' no field can not be empty. If set to '0' any or all fields can be empty.
$req = '0';

// --------------------------Do not edit below this line--------------------------

// Subject of confirmation email.
$conf_subject = 'Your recent enquiry';

// Who should the confirmation email be from?
$conf_sender = 'teamtesla02@gmail.com';

$msg = $_POST['Name'] . ",\n\nThank you for your recent enquiry. A member of our 
team will respond to your message as soon as possible.";

mail( $_POST['Email'], $conf_subject, $msg, 'From: ' . $conf_sender );