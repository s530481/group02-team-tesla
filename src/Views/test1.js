var webdriver=require('selenium-webdriver');
By = webdriver.By,
until = webdriver.until;

var driver=new webdriver.Builder().forBrowser('chrome').build();
driver.get('http://127.0.0.1:8082/attendee');
driver.sleep(10000);
driver.findElement(By.name("firstName")).sendKeys("fish");
driver.findElement(By.name("lastName")).sendKeys("box");
driver.findElement(By.name("email")).sendKeys("testingtheapp2@gmail.com");
driver.findElement(By.name("contactNumber")).sendKeys("1234565655");
driver.findElement(By.name("address")).sendKeys("nasa");
driver.findElement(By.name("institutionName")).sendKeys("studioinc");
driver.findElement(By.name("institutionCity")).sendKeys("Ugandas");
driver.findElement(By.name("institutionState")).sendKeys("andamans");
driver.findElement(By.name("zipcode")).sendKeys("54655");
driver.findElement(By.name("country")).sendKeys("wrogn");
driver.findElement(By.name("positionTitle")).sendKeys("exCEO");
driver.findElement(By.xpath("//button[contains(text(),'to Payment')]")).click();
driver.sleep(10000);