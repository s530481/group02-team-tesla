//Import the mongoose module
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// Define schema
var proposalSchema = new Schema({
    firstName: String,
    lastName:String,
    positionTitle:String,
    institution:String,
    city:String,
    state:String,
    country:String,
    zipcode:String,
    emailAddress:String,
    contactNumber:Number,
    equimentNeeds:String,
    softwareNeeds:String,
    paper: String,   
    sessionTitle: String,
    description: String, 
    dietaryRestrictions: String,
    track:String,
    accept:String,
    reject:String,
    filepath:String
});


module.exports = mongoose.model('proposals', proposalSchema);