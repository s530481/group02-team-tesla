var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// Define schema
var breakOutSessionSchema = new Schema({
    BreakOut_Session: Number,
    Time: String,   
    Activity: String,
    Description: String,
    Location: String
});

module.exports = mongoose.model('breakOutSession', breakOutSessionSchema);