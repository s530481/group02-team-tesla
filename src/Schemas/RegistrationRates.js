var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// Define schema
var registrationRates = new Schema({
    registrationRate: {type:String,required:true},
    registrationFee:{type:String,required:true},
    whatIncluded: {type:String,required:true},   
    
});

module.exports = mongoose.model('registrationRates', registrationRates);