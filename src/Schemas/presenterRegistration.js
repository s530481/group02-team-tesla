//Import the mongoose module
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// Define schema
var presenterSchema = new Schema({
    Presenter_FirstName: String,
    Presenter_LastName: String,
    Presenter_Email_Address: String,   
    Presenter_Contact_Number: Number,
    Presenter_Address: String, 
    // Name_of_School: String,
    // Level_of_Education: String,   
    Presenter_Position_Title: String,         
    Presenter_Dietary_Restrictions: String,
    programs: String,
    payment:String
});


module.exports = mongoose.model('presenterRegistration', presenterSchema);
// var registrationSchema = db.model('Registration', rSchema);