//Import the mongoose module
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// Define schema
var deadlineSchema = new Schema({
    programName: {type:String,required:true},
    date: {type:String,required:true},      
});

module.exports = mongoose.model('deadlines', deadlineSchema);