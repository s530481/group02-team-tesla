//Import the mongoose module
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// Define schema
var attendeeSchema = new Schema({
    firstName: String,
    lastName: String,
    email: String,   
    contactNumber: Number,
    address: String, 
    institutionName: String,
    institutionCity:String,
    institutionState:String,
    zipcode:String,
    country:String,
    positionTitle: String,
    dietaryRestrictions: String,
    programs: String,
    payment:String
});

module.exports = mongoose.model('attendeeRegistration', attendeeSchema);
// var registrationSchema = db.model('Registration', rSchema);