//Import the mongoose module
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// Define schema
var studentSchema = new Schema({
    firstName: String,
    lastName:String,
    email: String,   
    contactNumber: Number,
    address: String, 
    institutionName: String,
    institutionCity: String,   
    institutionState: String,  
    zipcode:String,
    country:String,
    dietaryRestrictions: String,
    programs: String,
    payment:String
});


module.exports = mongoose.model('studentRegistration', studentSchema);
// var registrationSchema = db.model('Registration', rSchema);