//Import the mongoose module
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// Define schema
var facultySchema = new Schema({
    firstName: String,
    lastName: String,
    email: String,   
    contactNumber: Number,
    address: String, 
    institutionName: String,
    institutionCity:String,
    institutionState:String,
    zipcode:String,
    country:String,
    positionTitle: String,
    dietaryRestrictions: String,
    programs: String
});


module.exports = mongoose.model('facultyRegistration', facultySchema);
// var registrationSchema = db.model('Registration', rSchema);