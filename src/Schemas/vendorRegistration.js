//Import the mongoose module
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// Define schema
var vendorSchema = new Schema({
    VendorType:String,
    companyName: String,
    city: String,
    state: String,   
    zipcode: Number,
    websiteURL: String,  
    contactName: String,  
    contactTitle: String,
    contactPhone: String,
    contactEmail:String,
    confirmEmail:String,
    dietaryRestrictions : String,
    programs:String,
    payment:String
});


module.exports = mongoose.model('vendorRegistration', vendorSchema);
// var registrationSchema = db.model('Registration', rSchema);