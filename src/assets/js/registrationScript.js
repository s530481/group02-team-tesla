document.getElementById('allRegistration').hidden=true;
document.getElementById('allsky').hidden=true;
document.getElementById('studentform').hidden=true;
document.getElementById('allFac').hidden=true;
document.getElementById('Vendor').hidden=true;
document.getElementById('reselectCategory').hidden=true;
document.getElementById('proceed').hidden=true;

function attendee(){
    document.getElementById('allRegistration').hidden=false;
    // document.getElementsByClassName('row1');

    // document.getElementById('radioButtons').getElementsByName('vRegistration').checked=false;
    // document.getElementsByName('FacultyRegistration').checked=true;
    document.getElementById('radioButtons').hidden=true;
    document.getElementById('reselectCategory').hidden=false;
    document.getElementById('proceed').hidden=false;

}
function presenter(){
    document.getElementById('allsky').hidden=false;
    // document.getElementsByClassName('row1');

    // document.getElementById('radioButtons').getElementsByName('vRegistration').checked=false;
    // document.getElementsByName('FacultyRegistration').checked=true;
    document.getElementById('radioButtons').hidden=true;
    document.getElementById('reselectCategory').hidden=false;
    function myFunction(){
        var x = document.getElementById("myFile");
        var txt = "";
        if ('files' in x) {
            if (x.files.length == 0) {
                txt = "Select one or more files.";
            } else {
                for (var i = 0; i < x.files.length; i++) {
                    txt += "<br><strong>" + (i+1) + ". file</strong><br>";
                    var file = x.files[i];
                    if ('name' in file) {
                        txt += "name: " + file.name + "<br>";
                    }
                    if ('size' in file) {
                        txt += "size: " + file.size + " bytes <br>";
                    }
                }
            }
        } 
        else {
            if (x.value == "") {
                txt += "Select one or more files.";
            } else {
                txt += "The files property is not supported by your browser!";
                txt  += "<br>The path of the selected file: " + x.value; // If the browser does not support the files property, it will return the path of the selected file instead. 
            }
        }
        document.getElementById("demo").innerHTML = txt;
    }
    document.getElementById('proceed').hidden=false;
}

function student(){
    document.getElementById('studentform').hidden=false;
    // document.getElementsByClassName('row1');

    // document.getElementById('radioButtons').getElementsByName('vRegistration').checked=false;
    // document.getElementsByName('FacultyRegistration').checked=true;
    document.getElementById('radioButtons').hidden=true;
    document.getElementById('reselectCategory').hidden=false;
    document.getElementById('proceed').hidden=false;

}

function vendor(){
    document.getElementById('Vendor').hidden=false;

    // document.getElementsByName('FacultyRegistration').checked=true;
    // document.getElementsByName('SingleRegistration').checked=true;
    document.getElementById('radioButtons').hidden=true;
    document.getElementById('reselectCategory').hidden=false;
    document.getElementById('proceed').hidden=false;
}

function faculty(){
    document.getElementById('allFac').hidden=false;

    // document.getElementsByName('VendorRegistration').checked=true;
    // document.getElementsByName('SingleRegistration').checked=true;
    document.getElementById('radioButtons').hidden=true;
    document.getElementById('reselectCategory').hidden=false;
    document.getElementById('proceed').hidden=false;
}

function selectCategory(){
    document.getElementById('radioButtons').hidden=false;
    document.getElementById('allRegistration').hidden=true;
    document.getElementById('allsky').hidden=true;
    document.getElementById('proceed').hidden=true;
    document.getElementById('studentform').hidden=true;
    document.getElementById("aRegistration").checked = false;
    document.getElementById("sRegistration").checked = false;
    document.getElementById("vRegistration").checked = false;
    document.getElementById("fRegistration").checked = false;
    document.getElementById("pRegistration").checked = false;
    reset();
}