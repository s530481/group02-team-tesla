
var deadline = "";
function modifyDeadlines(target,text,dead) {
    console.log(dead);
    var deadlineOld = document.getElementById(target.id).innerHTML;
 //   console.log(deadlineOld+"   key "+ program);
    var newValue = prompt(text);
        if (newValue == "" || newValue == null) {
            deadline = deadlineOld;
        }
        else {
                      deadline = newValue; 
        }  
    document.getElementById(target.id).innerHTML = deadline;
    document.getElementById(dead).value = deadline;
   // saveDeadlines(program, deadline);    
}

var newFee = "";
function modifyRegistrationFee(target,text,fee) {
    var oldValue = document.getElementById(target.id).innerHTML;
    var newValue = prompt(text);
        if (newValue == "" || newValue == null) {
            newFee = oldValue;
        }
        else {
                      newFee = newValue; 
        }  
    document.getElementById(target.id).innerHTML = newFee;
    document.getElementById(fee).value = newFee;
}

function exportToExcel() {
    var i, j;
    var csv = "";

    var table = document.getElementById("edit");

    var table_headings = table.children[0].children[0].children;
    var table_body_rows = table.children[1].children;

    var heading;
    var headingsArray = [];
    for (i = 0; i < table_headings.length; i++) {
        heading = table_headings[i];
        console.log(heading)
        headingsArray.push('"' + heading.innerHTML + '"');
    }

    csv += headingsArray.join(',') + "\n";

    var row;
    var columns;
    var column;
    var columnsArray;
    for (i = 0; i < table_body_rows.length; i++) {
        row = table_body_rows[i];
        columns = row.children;
        columnsArray = [];
        for (j = 0; j < columns.length; j++) {
            var column = columns[j];
            columnsArray.push('"' + column.innerHTML + '"');
        }
        csv += columnsArray.join(',') + "\n";
    }

    download("export.csv", csv);
}

//From: http://stackoverflow.com/a/18197511/2265487
function download(filename, text) {
    var pom = document.createElement('a');
    pom.setAttribute('href', 'data:text/csv;charset=utf-8,' + encodeURIComponent(text));
    pom.setAttribute('download', filename);

    if (document.createEvent) {
        var event = document.createEvent('MouseEvents');
        event.initEvent('click', true, true);
        pom.dispatchEvent(event);
    }
    else {
        pom.click();
    }
}

