var mongoose = require('mongoose');
// mongoose.connect('mongodb://localhost:27017/test');
var db = mongoose.createConnection('localhost:27017', 'test');
var Schema = mongoose.Schema;


var rSchema = new mongoose.Schema({
    Attendee_Name: String,
    Email_Address: String,   
    Contact_Number: Number,
    Address: String, 
    Name_of_School: String,
    Level_of_Education: String,   
    Job_Title: String,         
    Dietary_Restrictions: String

});
// module.exports = mongoose.model('Registration', registrationSchema);
var registrationSchema = db.model('Registration', rSchema);